const dbConn = require("../config/db");

var Candidate = function (candidate) {
  this.name = candidate.name;
  this.email = candidate.email;
  this.address = candidate.address;
  this.created_at = new Date();
};
Candidate.create = function (newEmp, result) {
  dbConn.query("INSERT INTO candidate set ?", newEmp, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
    } else {
      console.log(res.insertId);
      result(null, res.insertId);
    }
  });
};
Candidate.findAll = function (result) {
  dbConn.query(
    "Select id,name,email,address from candidate order by id DESC",
    function (err, res) {
      if (err) {
        console.log("error: ", err);
        result(null, err);
      } else {
        console.log("employees : ", res);
        result(null, res);
      }
    }
  );
};

Candidate.getMaxScoredCandidate = (result) => {
  dbConn.query(
    "SELECT c.id,c.name,(ts.first_round+ts.second_round+ts.third_round) as sum_of_mark from candidate c LEFT JOIN test_score ts on c.id=ts.candidate_id order by sum_of_mark DESC LIMIT 0,1",
    function (err, res) {
      if (err) {
        console.log("error: ", err);
        result(null, err);
      } else {
        console.log("candidate : ", res);
        result(null, res);
      }
    }
  );
};

Candidate.getTotAvgMarkEachRound = (result) => {
  dbConn.query(
    "SELECT avg(ts.first_round) avg_mark_firstround,avg(ts.second_round) avg_mark_secondround,avg(ts.third_round) avg_mark_thirdround  from candidate c LEFT JOIN test_score ts on c.id=ts.candidate_id",
    function (err, res) {
      if (err) {
        console.log("error: ", err);
        result(null, err);
      } else {
        console.log("candidate : ", res);
        result(null, res);
      }
    }
  );
};

module.exports = Candidate;
