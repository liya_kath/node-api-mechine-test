var express = require("express");
var router = express.Router();
const CandidateController = require("../controller/candidate.controller");

//api 1 add candidate
router.post("/add_candidate", CandidateController.create);
//api 2 assign scores
router.post("/assign_score", CandidateController.assignScore);
//api 3 candidate list for assign score
router.post("/candidate_list", CandidateController.candidateList);
//api 4 find maximum score
router.post(
  "/find_max_scored_candidate",
  CandidateController.getMaxScoredCandidate
);
//api 5 find total avg marks by round wise(all candidates)
router.post(
  "/get_total_avg_mark_by_round",
  CandidateController.getTotAvgMarkEachRound
);

module.exports = router;
