const dbConn = require("../config/db");

var Marks = function (marks) {
  this.candidate_id = marks.candidate_id;
  this.first_round = marks.first_round;
  this.second_round = marks.second_round;
  this.third_round = marks.third_round;
};

Marks.create = function (newMrk, result) {
  dbConn.query("INSERT INTO test_score set ?", newMrk, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
    } else {
      console.log(res.insertId);
      result(null, res.insertId);
    }
  });
};

module.exports = Marks;
