//import { Candidate } from "../model/candidate.model";
const Candidate = require("../model/candidate.model");
const Marks = require("../model/marks.model");

exports.create = function (req, res) {
  const new_candidate = new Candidate(req.body);

  //handles null error
  if (
    (req.body.constructor === Object && Object.keys(req.body).length === 0) ||
    req.body.email == "" ||
    req.body.address == "" ||
    req.body.name == ""
  ) {
    res
      .status(400)
      .send({ error: true, message: "Please provide all required field" });
  } else {
    Candidate.create(new_candidate, function (err, candidate) {
      if (err) res.send("Error");
      res.json({
        error: false,
        message: "Candidate added successfully!",
        data: candidate,
      });
    });
  }
};

exports.assignScore = function (req, res) {
  const new_marks = new Marks(req.body);

  //handles null error
  if (
    (req.body.constructor === Object && Object.keys(req.body).length === 0) ||
    req.body.candidate_id == "" ||
    req.body.first_round == "" ||
    req.body.second_round == "" ||
    req.body.third_round == ""
  ) {
    res
      .status(400)
      .send({ error: true, message: "Please provide all required field" });
  } else {
    Marks.create(new_marks, function (err, result) {
      if (err) res.send(err);
      res.json({
        error: false,
        message: "Mark assigned successfully!",
        data: result,
      });
    });
  }
};

exports.candidateList = (req, res) => {
  Candidate.findAll(function (err, result) {
    console.log("controller");
    if (err) res.send(err);
    console.log("res", result);
    res.send(result);
  });
};

exports.getMaxScoredCandidate = (req, res) => {
  Candidate.getMaxScoredCandidate(function (err, result) {
    console.log("controller");
    if (err) res.send(err);
    console.log("res", result);
    res.send(result);
  });
};

exports.getTotAvgMarkEachRound = (req, res) => {
  Candidate.getTotAvgMarkEachRound(function (err, result) {
    if (err) res.send(err);
    console.log("res", result);
    res.send(result);
  });
};

//getMaxScoredCandidate
